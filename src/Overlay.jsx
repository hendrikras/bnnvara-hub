import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Portal from './Portal';

export default class Overlay extends PureComponent {
  static propTypes = {
    style: PropTypes.object,
    className: PropTypes.string,
    active: PropTypes.bool,
    visible: PropTypes.bool.isRequired,
    renderNode: PropTypes.object,
    onClick: PropTypes.func,
  };

  static childContextTypes = {
    isInPortal: PropTypes.bool,
  };

  getChildContext() {
    return { isInPortal: false };
  }

  render() {
    const { active, visible, renderNode, onClick, style, className } = this.props;
    return (
      <Portal visible={visible} renderNode={renderNode}>
        <div
          style={style}
          className={className}
          onClick={onClick}
        />
      </Portal>
    );
  }
}