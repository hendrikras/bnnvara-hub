import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';


function isBetween(num, min, max) {
  return num >= min && num <= max;
}

function between(validator, min, max) {
  return function validate(props, propName, componentName, location, propFullName, ...args) {
    const componentNameSafe = componentName || '<<anonymous>>';
    const propFullNameSafe = propFullName || propName;

    let err = validator(props, propName, componentName, location, propFullName, ...args);
    const value = props[propName];
    if (!err && typeof value !== 'undefined' && !isBetween(value, min, max)) {
      err = new Error(
        `You provided a \`${propFullNameSafe}\` ${location} to the ${componentNameSafe} that was ` +
        `not within the range from '${min} - ${max}'. \`${propFullNameSafe}\`: ${value}.`
      );
    }

    return err;
  };
}


/**
 * The `Paper` component is a simple wrapper that adds box-shadow.
 *
 * You can also use the SCSS mixin instead of paper.
 *
 * ```scss
 * @include md-box-shadow(5);
 * ```
 */
export default class Paper extends PureComponent {
  static propTypes = {
    /**
     * The component to render the paper as.
     */
    component: PropTypes.oneOfType([
      PropTypes.func,
      PropTypes.string,
      PropTypes.object,
    ]).isRequired,

    /**
     * An optional className to apply.
     */
    className: PropTypes.string,

    /**
     * The depth of the paper. This should be a number between 0 - 5. If
     * the depth is 0, it will raise to a depth of 3 on hover.
     */
    zDepth: between(PropTypes.number.isRequired, 0, 5),

    /**
     * Any children to display in the paper.
     */
    children: PropTypes.node,

    /**
     * Boolean if the paper should raise to the `zDepth` of `3` on hover when the initial
     * `zDepth` is `0`.
     */
    raiseOnHover: PropTypes.bool,
  };

  static defaultProps = {
    zDepth: 1,
    component: 'div',
  };

  render() {
    const { component: Component, zDepth, className, raiseOnHover, ...props } = this.props;

    return (
      <Component
        {...props}
        className={className}
      />
    );
  }
}