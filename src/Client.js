function fetchData(baseUrl, graphQuery) {
  return fetch(baseUrl, {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({
      query: graphQuery,
    }),
  })
    .then(response => response.json())
    .then((response) => {
      if (response.message) {
        return Promise.reject(new Error(response.message));
      }
      return response;
    });
}

export const fuzzySearch = (baseUrl, query, amount, fields, from) => fetchData(baseUrl, `{ autocomplete(keyword: "${query}", from: ${from}, sort: "relevance", order: "desc", size: ${amount}) { ${fields} }}`);
export const search = (baseUrl, searchTerm, parameters, fields) => fetchData(baseUrl, `{ search(keyword: "${searchTerm}", ${parameters} ) { hits results{ ${fields} }} }`);
