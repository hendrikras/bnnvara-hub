import React, { useState } from 'react';
import Drawer from 'rc-drawer';
import  './drawer.scss';
import styles from './styles.scss';

import Logo from './img/logo-BNNVARA.svg';
import Avatar from 'react-avatar';
import {fuzzySearch, search} from './Client'
import Search from '@bnnvara/search'

const style = {
  padding: '5px',
  display: 'flex',
  justifyContent:'space-between',
  height: '60px',
  background: 'black',
  color: 'white'
}

const logo = {
  position: 'relative',
  top: '-20px'
}

const button = {
  outline: 'none',
  background: 'black',
  color: 'white',
  border: 0,
  font: 'BNNVARA-Bold,sans-serif',
  fontSize: 20,
  fontWeight: 850
}

const avatar = {
  position: 'relative',
  top: '10px',
  right: '10px'
}

export default function(props) {
  const [ toggle, setToggle ] = useState(false);
  const getButton = bool =>
    (<button style={button} onClick={() => setToggle(bool)}><Logo style={logo} width={150} height={100} /></button>);

  return (
    <div style={style}>
      { getButton(true) }
      <Avatar style={avatar} size={40} src="http://www.gravatar.com/avatar/2581bdac8e65ebb35c8ca3570d14c0bd" round={true}/>
      <Drawer handler={false}
              placement="top"
              height="80%"
              open={toggle}
              onMaskClick={() => setToggle(false)}
      >
        { getButton(false)}
        <Search
          fuzzySearch={fuzzySearch}
          search={search}
          baseUrl="http://localhost:9002/graphql"
        />
      </Drawer>
    </div>
  )
};
